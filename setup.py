#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name="django-jsonrpc2",
    version="0.1.0",
    packages=find_packages(exclude=["tests"]),
    package_data = {
        '': ['templates/jsonrpc2/*.html'],
    },
    zip_safe=False,
    author="Sergey Blinov",
    author_email="blinovsv@gmail.com",
    install_requires=[
        "docutils>=0.11",
        "pygments>=1.6",
        "requests",
    ],
    url="",
    description="",
    long_description="",
)
