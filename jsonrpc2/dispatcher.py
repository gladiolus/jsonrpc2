from collections import defaultdict, OrderedDict
import inspect
import json
import logging
from types import NoneType

from docutils.core import publish_parts

from exceptions import *


logger = logging.getLogger(__name__)


class JsonRpcDispatcher(object):
    _json_rpc_methods = {}

    def register_method(self, name, method, **options):
        argspec = options.pop("argspec", None) or inspect.getargspec(method)

        self._json_rpc_methods[name] = {
            "method": method,
            "docstring": method.__doc__,
            "argspec": argspec,
            "options": options,
        }

    def get_methods_permissions(self):
        return [k for k, v in self._json_rpc_methods.iteritems()
                if v.get("options", {}).get("login_required")]

    def get_methods_info(self):
        info = defaultdict(list)
        for name, data in self._json_rpc_methods.viewitems():
            docstring = data["docstring"]
            if docstring:
                docstring = publish_parts(docstring, writer_name='html')

            params = data["argspec"].args
            if data['options'].get('bind'):
                params = params[1:]

            example = {
                "jsonrpc": "2.0",
                "id": "test",
                "params": params,
                "method": name,
            }

            namespace = name.split('.', 1)[0]

            info[namespace].append({
                "name": name,
                "namespace": namespace,
                "docstring": docstring,
                "options": data.get("options", {}),
                "example": json.dumps(example, indent=4),
                "params": params,
            })

        for namespace in info:
            info[namespace] = sorted(info[namespace], key=lambda x: x["name"])

        info = OrderedDict(sorted(info.items(), key=lambda x: x[0]))

        return info

    def handle_rpc_request(self, raw_data, extra=None):
        try:
            try:
                request = json.loads(raw_data)
            except ValueError:
                raise ParseError()

            if isinstance(request, dict):
                # TODO: handle notification-like requests
                response = self._process_request(request, extra)

            elif isinstance(request, list):
                response = self._process_batch_request(request, extra)

            else:
                raise ParseError()

        except BaseRpcException as e:
            response = self._error_response(e.code, e.message, e.data, e.id)
            logger.exception(e)
        except BaseHttpException as e:
            raise e
        except Exception as e:
            response = self._error_response(500, e.message)
            logger.exception(e)

        return json.dumps(response)

    def _process_batch_request(self, requests, extra=None):
        responses = []
        for request in requests:
            response = self._process_request(request, extra)
            responses.append(response)
        return responses

    def _process_request(self, request, extra=None):
        id_ = None
        try:
            # handle type
            if not isinstance(request, dict):
                raise InvalidRequest()

            # handle jsonrpc version
            if request.get("jsonrpc") != "2.0":
                raise InvalidRequest()

            # handle id
            id_ = None
            if "id" in request:
                id_ = request["id"]
                if not isinstance(id_, (basestring, int, NoneType)):
                    raise InvalidRequest()

            # handle method
            method_name = request.get("method")
            if method_name is None or not isinstance(method_name, basestring):
                raise InvalidRequest()

            try:
                method_data = self._json_rpc_methods[method_name]
                method = method_data["method"]
                options = method_data["options"]
            except KeyError:
                raise MethodNotFound()

            # handle params
            params = request.get('params')
            if not isinstance(params, (list, dict, NoneType)):
                raise InvalidRequest()

            args = []
            if options.get('bind'):
                args += [extra]
            kwargs = {}
            if isinstance(params, list):
                args += params
            elif isinstance(params, dict):
                kwargs = params

            # run method
            try:
                result = method(*args, **kwargs)
                response = self._response(result, id_)
            except TypeError:
                raise InvalidParams()
        except BaseRpcException as e:
            response = self._error_response(e.code, e.message, e.data, id_)
            logger.exception(e)
        except BaseHttpException as e:
            raise e
        except Exception as e:
            response = self._error_response(500, str(e), id_=id_)
            logger.exception(e)

        return response

    @staticmethod
    def _error_response(code, message, data=None, id_=None):
        error = {
            "code": code,
            "message": message,
        }
        if data is not None:
            error["data"] = data
        return {
            "jsonrpc": "2.0",
            "error": error,
            "id": id_,
        }

    @staticmethod
    def _response(result, id_=None):
        return {
            "jsonrpc": "2.0",
            "result": result,
            "id": id_,
        }


dispatcher = JsonRpcDispatcher()
