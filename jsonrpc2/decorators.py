from jsonrpc2.dispatcher import dispatcher


def jsonrpc_method(name, **options):

    def wrapper(method):
        dispatcher.register_method(name, method, **options)
        return method

    return wrapper
