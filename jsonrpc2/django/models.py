from django.db import models
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType


CONTENT_TYPE_NAME = "rpc permission"


class RpcPermissionManager(models.Manager):
    def get_queryset(self):
        qs = super(RpcPermissionManager, self).get_queryset()
        qs = qs.filter(content_type__name=CONTENT_TYPE_NAME)
        return qs


class RpcPermission(Permission):
    objects = RpcPermissionManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        ct, created = ContentType.objects.get_or_create(
            name=CONTENT_TYPE_NAME, app_label=self._meta.app_label
        )
        self.content_type = ct
        super(RpcPermission, self).save(*args, **kwargs)
