"""
    TODO: implement this module as a class for more flexibility
"""

import functools
import inspect

from django.core.exceptions import FieldError

from jsonrpc2.exceptions import Http403, InvalidParams, PermissionDenied
from jsonrpc2.decorators import jsonrpc_method as base_jsonrpc_method


def jsonrpc_method(name, login_required=True,
                   extra_permissions=None, **options):

    options.update(dict(
        bind=True,
        login_required=login_required,
        extra_permissions=extra_permissions,
    ))

    def wrapper(method):
        options['argspec'] = inspect.getargspec(method)

        @base_jsonrpc_method(name, **options)
        @functools.wraps(method)
        def django_jsonrpc_view(request, *args, **kwargs):
            if login_required:
                if not request.user.is_authenticated():
                    raise Http403
                if not request.user.is_active:
                    raise Http403

                permissions = ['django.{}'.format(name)]
                if extra_permissions and isinstance(extra_permissions, list):
                    permissions += extra_permissions

                try:
                    if not request.user.has_perms(permissions):
                        raise PermissionDenied
                except:
                    raise PermissionDenied

            try:
                return method(request, *args, **kwargs)
            except FieldError:
                raise InvalidParams

        return django_jsonrpc_view

    return wrapper


def jsonrpc_form(form, qs=False, **options):
    options.update(dict(
        form=form,
        qs=qs
    ))

    def wrapper(method):
        def do(request, data):
            pk_name = form._meta.model._meta.pk.name
            pk = data.get(pk_name)

            instance = None
            if pk:
                instance = form._meta.model.get_or_not_found(pk, request.user)

            f = form(data, instance=instance)

            if not f.is_valid():
                raise InvalidParams(data=f.errors)
            f.save(commit=False)
            return f

        @functools.wraps(method)
        def django_jsonrpc_form_view(request, data, *args, **kwargs):
            if qs:
                new_data = []
                for d in data:
                    new_data.append(do(request, d))
                f = new_data
            else:
                f = do(request, data)
            return method(request, f)

        return django_jsonrpc_form_view

    return wrapper
