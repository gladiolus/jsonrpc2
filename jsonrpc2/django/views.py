from django.http import HttpResponse
from django.http import HttpResponseBadRequest  # 400
from django.http import HttpResponseForbidden  # 403
from django.http import HttpResponseNotFound  # 404
from django.http import HttpResponseNotAllowed  # 405
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.conf import settings

from jsonrpc2.dispatcher import dispatcher as jsonrpc_dispatcher
from jsonrpc2.exceptions import *


class RpcView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(RpcView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def get(request):
        if not settings.DEBUG:
            return HttpResponseNotFound()
        return render_to_response("jsonrpc2/base.html", {
            "jsonrpc_methods": jsonrpc_dispatcher.get_methods_info()
        })

    @staticmethod
    def post(request):
        response = HttpResponse()

        try:
            response_data = jsonrpc_dispatcher.handle_rpc_request(
                request.body, extra=request)
            response["Content-Type"] = "application/json-rpc"
            response.write(response_data)
        except Http400:
            response = HttpResponseBadRequest()
        except Http403:
            response = HttpResponseForbidden()
        except Http404:
            response = HttpResponseNotFound()
        except Http405:
            response = HttpResponseNotAllowed()

        return response
