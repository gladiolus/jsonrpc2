from importlib import import_module

from django.utils.module_loading import module_has_submodule


def simple_autodiscover_modules(*args, **kwargs):
    from django.conf import settings

    for app in settings.INSTALLED_APPS:
        for module_to_search in args:
            try:
                import_module('%s.%s' % (app, module_to_search))
            except:
                app_module = import_module(app)
                if module_has_submodule(app_module, module_to_search):
                    raise
